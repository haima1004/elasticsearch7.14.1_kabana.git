## 版本：
elasticsearch7.14.1 (已经集成了ik分词器)
kabana7.14.1
elasticsearch-head #需要安装nodejs


## 安装说明：
https://www.cnblogs.com/haima/p/15817428.html

## 赋予权限
当前目录下所有文件赋予权限(读、写、执行)
chmod -R 777 ./es7-kabana

## 常用命令：

```sh
docker-compose up -d #后台启动
docker-compose down  #停止
```
注意，如果启动不成功，需要删除容器，把所有文件和文件夹赋予权限(读、写、执行)，再启动。

```sh
[root@HmEduCentos01 docker]# docker ps -a
CONTAINER ID   IMAGE                                                              COMMAND                  CREATED          STATUS          PORTS                                                                                  NAMES
1c5b971d99c3   registry.cn-hangzhou.aliyuncs.com/zhengqing/kibana:7.14.1          "/bin/tini -- /usr/l…"   41 minutes ago   Up 41 minutes   0.0.0.0:5601->5601/tcp, :::5601->5601/tcp                                              kibana
3ca20dcf4bd4   registry.cn-hangzhou.aliyuncs.com/zhengqing/elasticsearch:7.14.1   "/bin/tini -- /usr/l…"   41 minutes ago   Up 41 minutes   0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 0.0.0.0:9300->9300/tcp, :::9300->9300/tcp   elasticsearch
1af7cf5fd1ad   wallbase/elasticsearch-head:6-alpine                               "/bin/sh -c 'node_mo…"   41 minutes ago   Up 41 minutes   0.0.0.0:9100->9100/tcp, :::9100->9100/tcp                                              elasticsearch-head

```

## 访问

ES访问地址：ip地址:9200
默认账号密码：elastic/123456 #未设置即为空
kibana访问地址：ip地址:5601/app/dev_tools#/console
默认账号密码：elastic/123456 #未设置即为空
elasticsearch-head地址：ip地址:9100




## logstash软件下载

华为源
这里我下载:logstash-7.14.1-linux-x86_64.tar.gz  大家根据自己的系统下载对应的版本
https://mirrors.huaweicloud.com/logstash/7.14.1/

官网地址:
https://www.elastic.co/cn/downloads/past-releases/logstash-7-14-1
这里我下载Linux x86_64的,大家根据自己的系统下载对应的版本

## logstash导入movies.csv数据

1. 进入`/elasticsearch7.14.1_kabana/logstash-7.14.1`目录
2. 新建名为 logstash.conf 的文件.

```sh
input {
  file {
    # 引号的的内容为 movies.csv 的实际路径，根据实际情况
    path => "/home/haima/local/docker/es-kabana/logstash-7.14.1/movies.csv"
    start_position => "beginning"
    # 日志目录
    sincedb_path => "/home/haima/local/docker/es-kabana/logstash-7.14.1/db_path.log"
  }
}
filter {
  csv {
    separator => ","
    columns => ["id","content","genre"]
  }

  mutate {
    split => { "genre" => "|" }
    remove_field => ["path", "host","@timestamp","message"]
  }

  mutate {

    split => ["content", "("]
    add_field => { "title" => "%{[content][0]}"}
    add_field => { "year" => "%{[content][1]}"}
  }

  mutate {
    convert => {
      "year" => "integer"
    }
    strip => ["title"]
    remove_field => ["path", "host","@timestamp","message","content"]
  }

}
output {
   elasticsearch {
     # 双引号中的内容为ES的地址，视实际情况而定
     hosts => "http://localhost:9200"
     index => "movies"
     document_id => "%{id}"
   }
  stdout {}
}


```

3. 执行导入命令:
打开dos命令行，进入到 logstash 的 bin 目录下，执行如下命令导入 movies 的数据

#linux命令
`logstash -f ../config/logstash.conf`

#linux命令
`logstash.bat -f D:\logstash-datas\config\logstash.co`


2.4.3 验证进入到 kibana 的命令行页面，执行 `GET _cat/indices` 验证数据是否成功